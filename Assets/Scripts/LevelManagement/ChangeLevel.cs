using UnityEngine;

public class ChangeLevel : MonoBehaviour
{
    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_CambioNivel;
    private bool m_Activado;

    [Header("Referencias a objetos")]
    private int m_PlayerLayer;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Activado = false;
        m_PlayerLayer = LayerMask.NameToLayer("Player");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerLayer && !m_Activado)
        {
            print("cambio nivel");
            m_Activado = true;
            m_CambioNivel.Raise();
        }
    }
}
