using UnityEngine;

public class ParedTrampa : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject paredTrampa;

    //----------------------------------[ EVENTOS }----------------------------------//
    public void ActivarParedTrampa()
    {
        paredTrampa.SetActive(true);
    }
}
