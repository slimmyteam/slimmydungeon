using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Int")]
public class GEGenericoInt : GEGenerico<int> { }
