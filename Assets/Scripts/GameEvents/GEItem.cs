using UnityEngine;
using static UnityEditor.Progress;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Item")]
public class GEItem : GEGenerico<SOItems> { }
