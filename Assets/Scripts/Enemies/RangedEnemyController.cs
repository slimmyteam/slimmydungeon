using System.Collections;
using UnityEngine;

[RequireComponent(typeof(DamageManager))]
public class RangedEnemyController : MonoBehaviour
{
    [Header("Componentes")]
    private Animator m_Animator;
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_Rigidbody;

    [Header("Parámetros del Ranged Enemy")]
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private bool m_Dead;
    private enum SwitchMachineStates { NONE, IDLE, FLEE, SHOOT, BURNED, HURT, DEAD };
    [SerializeField]
    private SwitchMachineStates m_EstadoActual;

    [Header("Disparo")]
    [SerializeField]
    private GameObject m_ShotPool;
    public GameObject ShootPool { get => m_ShotPool; set => m_ShotPool = value; }
    [SerializeField]
    private Transform m_ShotPosition;
    [SerializeField]
    private int m_ShotDamage;

    [Header("Areas de deteccion")]
    [SerializeField]
    private DetectionAreaController m_AreaHuir;
    [SerializeField]
    private DetectionAreaController m_SaleDelAreaHuir;
    [SerializeField]
    private DetectionAreaController m_AreaDisparar;
    [SerializeField]
    private DetectionAreaController m_SaleDelAreaDisparo;

    [Header("Variables de control de áreas")]
    [SerializeField]
    private bool heEntradoAreaDisparo;
    [SerializeField]
    private bool heEntradoAreaHuir;

    [Header("Scriptable Objects")]
    [SerializeField]
    private SOEnemy m_SOEnemy;
    public SOEnemy EnemyStats { get => m_SOEnemy; set => m_SOEnemy = value; }
    [SerializeField]
    private PartidaSO PartidaSO;

    [Header("Referencias a objetos")]
    private Transform m_PlayerTransform;

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoEsperaDisparo;
    [SerializeField]
    private float m_TiempoEsperaDisparoArea;
    private int m_PlayerAttacksLayer;

    [Header("Scripts")]
    private DamageManager m_DamageManager;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoGameObjectInt m_EnemyDead;
    [SerializeField]
    private GameEvent actualizarCoins;

    [Header("Corutinas")]
    private Coroutine m_disparando;
    private Coroutine m_esperaDisparo;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//

    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_DamageManager = this.gameObject.GetComponent<DamageManager>();

        m_Speed = m_SOEnemy.speed;
        m_ShotDamage = m_SOEnemy.damage;

        m_PlayerAttacksLayer = LayerMask.NameToLayer("PlayerAttacks");
    }

    private void OnEnable()
    {
        m_AreaHuir.OnDetected += EstadoHuir;
        m_SaleDelAreaHuir.OnExit += DejaDeHuir;
        m_AreaDisparar.OnDetected += Disparo;
        m_SaleDelAreaDisparo.OnExit += DejaDeDisparar;
        m_DamageManager.SO = m_SOEnemy;
        m_DamageManager.OnFire = false;
        m_Dead = false;
        IniciarEstado(SwitchMachineStates.IDLE);
        m_SpriteRenderer.color = Color.white;

        heEntradoAreaDisparo = false;
        heEntradoAreaHuir = false;
    }

    void Start()
    {
        IniciarEstado(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        ActualizarEstado();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerAttacksLayer)
        {
            m_Dead = m_DamageManager.LoseHealth(collision.gameObject.GetComponent<HitboxController>().damage, collision.gameObject.GetComponent<HitboxController>().type);
            if (m_Dead)
            {
                m_EnemyDead.Raise(collision.gameObject, EnemyStats.experience);
                ChangeState(SwitchMachineStates.DEAD);
            }
            else
                ChangeState(SwitchMachineStates.HURT);
        }
    }

    private void OnDestroy()
    {
        m_AreaHuir.OnDetected -= EstadoHuir;
        m_SaleDelAreaHuir.OnExit -= DejaDeHuir;
        m_AreaDisparar.OnDetected -= Disparo;
        m_SaleDelAreaDisparo.OnExit -= DejaDeDisparar;

        gameObject.SetActive(false);
    }

    //-------------------------------[ MÁQUINA DE ESTADOS ]-------------------------------//

    private void ChangeState(SwitchMachineStates nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == SwitchMachineStates.DEAD) return;

        FinalizarEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(SwitchMachineStates nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.FLEE:
                m_Rigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.SHOOT:
                m_Rigidbody.velocity = Vector2.zero;
                m_esperaDisparo = StartCoroutine(EsperaDisparoArea());
                break;

            case SwitchMachineStates.BURNED:
                m_Rigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.red;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("RangedEnemyHurt");
                break;

            case SwitchMachineStates.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                Die();
                break;

            default:
                break;
        }
    }

    private void ActualizarEstado()
    {
        if (m_EstadoActual != SwitchMachineStates.DEAD)
        {
            switch (m_EstadoActual)
            {
                case SwitchMachineStates.FLEE:
                    m_Rigidbody.velocity = (transform.position - m_PlayerTransform.position).normalized * m_Speed;

                    Encarar(m_Rigidbody.velocity);

                    break;
            }
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStates.SHOOT:
                if (m_esperaDisparo != null)
                    StopCoroutine(m_esperaDisparo);
                if (m_disparando != null)
                    StopCoroutine(m_disparando);
                break;
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.white;
                break;
        }
    }

    //-------------------------------[ CORUTINAS ]-------------------------------//

    private IEnumerator Disparando(Transform PlayerTransform)
    {
        while (true)
        {
            Encarar(PlayerTransform.position - transform.position);
            GameObject disparo = m_ShotPool.gameObject.GetComponent<ShotsPoolController>().RequestShot();
            disparo.GetComponent<HitboxController>().damage = m_SOEnemy.damage;
            disparo.GetComponent<HitboxController>().type = m_SOEnemy.damageType;

            disparo.transform.position = m_ShotPosition.position;

            disparo.SetActive(true);
            disparo.GetComponent<ShotController>().Init(m_ShotDamage, PlayerTransform.position);
            yield return new WaitForSeconds(m_TiempoEsperaDisparo);
        }
    }

    private IEnumerator EsperaDisparoArea()
    {
        yield return new WaitForSeconds(m_TiempoEsperaDisparoArea);
        m_disparando = StartCoroutine(Disparando(m_PlayerTransform));

    }

    //-------------------------------[ DELEGADOS AREAS DETECCION ]-------------------------------//

    public void EstadoHuir(Transform PlayerTransform)
    {
        heEntradoAreaHuir = true;
        heEntradoAreaDisparo = false;
        ChangeState(SwitchMachineStates.FLEE);
    }

    public void DejaDeHuir()
    {
        heEntradoAreaHuir = false;
        heEntradoAreaDisparo = true;
        ChangeState(SwitchMachineStates.SHOOT);
    }

    public void Disparo(Transform PlayerTransform)
    {
        m_PlayerTransform = PlayerTransform;
        heEntradoAreaDisparo = true;
        ChangeState(SwitchMachineStates.SHOOT);
    }

    public void DejaDeDisparar()
    {
        heEntradoAreaDisparo = false;
        ChangeState(SwitchMachineStates.IDLE);
    }

    //-----------------------------[ EVENTOS ANIMACIONES ]-------------------------------//
    public void FinalizaHurt()
    {
        if (heEntradoAreaDisparo)
        {
            ChangeState(SwitchMachineStates.SHOOT);

        }
        else if (heEntradoAreaHuir)
        {
            ChangeState(SwitchMachineStates.FLEE);
        }
        else
        {
            ChangeState(SwitchMachineStates.IDLE);
        }

    }

    private void Die()
    {
        if (PartidaSO.money + EnemyStats.money <= 99999) PartidaSO.money += EnemyStats.money;
        else PartidaSO.money = 99999;
        actualizarCoins.Raise();
        m_AreaHuir.OnDetected -= EstadoHuir;
        m_SaleDelAreaHuir.OnExit -= DejaDeHuir;
        m_AreaDisparar.OnDetected -= Disparo;
        m_SaleDelAreaDisparo.OnExit -= DejaDeDisparar;
        int r = Random.Range(0, EnemyStats.itemList.Length);
        GameObject item = Instantiate(EnemyStats.itemList[r]);
        item.transform.position = transform.position;
        gameObject.SetActive(false);
    }

    //-----------------------------[ FUNCIONES ]-------------------------------//

    private void Encarar(Vector3 direccion)
    {
        if (direccion.x < 0) //Si la x es menor a 0, rotamos
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            transform.eulerAngles = Vector3.zero;
        }
    }

    public void LlamarAMuerte()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }
}
