using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    [Header("Parámetros enemy pool")]
    [SerializeField]
    private List<GameObject> enemiesList;
    [SerializeField]
    private int poolSize;
    public int PoolSize { get => poolSize; set => poolSize = value; }

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject enemyPF;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//

    void Start()
    {
        Incialeitor(PoolSize);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    private void Incialeitor(int PoolSize)
    {
        enemiesList.Clear();
        AddEnemies(PoolSize);
    }

    private void AddEnemies(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject enemy = Instantiate(enemyPF, this.transform);
            enemy.SetActive(false);
            enemy.transform.parent = this.transform;
            enemiesList.Add(enemy);
        }
    }

    public GameObject Summon()
    {
        for (int i = 0; i < enemiesList.Count; i++)
        {
            if (!enemiesList[i].activeSelf)
            {
                enemiesList[i].SetActive(true);
                return enemiesList[i];
            }
        }
        AddEnemies(1);
        enemiesList[enemiesList.Count - 1].SetActive(true);
        return enemiesList[enemiesList.Count - 1];
    }
}
