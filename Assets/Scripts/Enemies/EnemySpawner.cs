using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("Componentes")]
    private AudioSource m_trumpetSound;

    [Header("Parámetros del Enemy Spawner")]
    [SerializeField]
    private Transform[] listaTransforms;
    [SerializeField]
    private GameObject m_poolEnemyMelee;
    public GameObject poolEnemyMelee { get { return m_poolEnemyMelee; } }
    [SerializeField]
    private GameObject m_poolEnemyRange;
    public GameObject poolEnemyRange { get { return m_poolEnemyRange; } }
    private int m_ContEnemies = 0;
    public int contEnemies { get { return m_ContEnemies; } }

    [Header("Scriptable objects")]
    [SerializeField]
    private SOEnemy SOEnemyMelee;
    [SerializeField]
    private SOEnemy SOEnemyRange;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoString m_lanzarMensaje;

    [Header("Corutinas")]
    private Coroutine m_esperaSpawner;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_trumpetSound = GetComponent<AudioSource>();
    }

    void Start()
    {
        Spawnear();
    }

    private void OnDestroy()
    {
        if (m_esperaSpawner != null) StopCoroutine(m_esperaSpawner);
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    public IEnumerator esperaSpawn()
    {
        yield return new WaitForSeconds(m_trumpetSound.clip.length);
        Spawnear();
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    public void Spawnear()
    {
        if (contEnemies == 0)
        {
            for (int i = 0; i < listaTransforms.Length; i++)
            {
                int r = Random.Range(0, 2);
                GameObject newEnemy;
                switch (r)
                {
                    case 0:
                        newEnemy = poolEnemyMelee.GetComponent<EnemyPool>().Summon();
                        newEnemy.transform.position = listaTransforms[i].position;
                        newEnemy.GetComponent<MeleeEnemyController>().EnemyStats = SOEnemyMelee;
                        break;
                    case 1:
                        newEnemy = m_poolEnemyRange.GetComponent<EnemyPool>().Summon();
                        newEnemy.transform.position = listaTransforms[i].position;
                        //newEnemy.GetComponent<RangedEnemyController>().ShootPool = poolEnemyRange;
                        newEnemy.GetComponent<RangedEnemyController>().EnemyStats = SOEnemyRange;
                        break;
                }
                m_ContEnemies++;
            }
        }
    }

    public void UnEnemigoMenos(GameObject arma, int exp)
    {
        m_ContEnemies--;
    }

    public void Trompetita()
    {
        if (m_ContEnemies > 0)
        {
            m_lanzarMensaje.Raise("No puedes usar la trompeta de farmeo cuando quedan enemigos vivos");
            return;
        }

        m_trumpetSound.Play();
        m_esperaSpawner = StartCoroutine(esperaSpawn());
    }
}
