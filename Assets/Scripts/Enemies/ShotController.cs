using System.Collections;
using UnityEngine;

public class ShotController : MonoBehaviour
{
    [Header("Componentes")]
    private Rigidbody2D m_Rigidbody;

    [Header("Parámetros del disparo")]
    [SerializeField]
    private float m_ShotSpeed = 5f;
    [SerializeField]
    private int m_DestroyTime = 2;

    [Header("Scripts")]
    [SerializeField]
    private HitboxController m_HitboxController;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//

    private void OnEnable()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_HitboxController = GetComponent<HitboxController>();
        StartCoroutine(DestruirShot());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            m_Rigidbody.velocity = Vector2.zero;
            gameObject.SetActive(false);
            ;           //StartCoroutine(ImpactoShot());
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Muros"))
        {
            gameObject.SetActive(false);
        }
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator DestruirShot()
    {
        yield return new WaitForSeconds(m_DestroyTime);
        gameObject.SetActive(false);
    }

    private IEnumerator chaserShot(int shotDamage, GameObject referenciaPlayer)
    {
        while (true)
        {
            Vector3 direction = (referenciaPlayer.transform.position - transform.position).normalized;
            transform.right = direction;
            m_Rigidbody.velocity = direction * m_ShotSpeed;
            m_HitboxController.damage = shotDamage;
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator ellipseMovement(Transform transfPlayer, Transform transfShotPosition, int shotDamage, float shootSpeed)
    {
        Vector3 vectorX = new Vector3(1f, 0f, 0f); //vector en el eje x
        float angle = Vector3.SignedAngle(vectorX, transfPlayer.right, Vector3.forward); //devuelve el angulo entre el player y el je de las x cuando dispara
        Vector3 posOriginPl = transfPlayer.position; //posicion original del player
        float timecounter = 0; // tiempo que tarda en hacer la funcion de sin
        float corutineTime = 0.1f; // tiempo que va pasando en cada bucle y se suma al timecounter
        float y = posOriginPl.y;// posicion y inicial del player
        this.transform.position = transfPlayer.position; //igualamos antes de iniciar el efecto la posicion de la bala y la del player (la de la bala pasa a la del player)
        Vector3 previousPOsition = Vector3.zero; // vector que indica la posicion previa en la que se encontraba la bala, comienza en (0,0,0)
        Quaternion rotation = Quaternion.Euler(0, 0, angle); //Quaternion que rota vectores en el angulo del player al disparar
        //-----while que dura un tiempo por defecto antes de romperse que depende del timecounter-----
        while (timecounter <= 1f)
        {
            timecounter += corutineTime;//aumentamos el timecounter la cantidad de corutineTime
            #region calculo de la transform
            y = Mathf.Sin(timecounter * Mathf.Rad2Deg); //la funcion del sin la realiza seg�n el time counter y pasa de radianes a degrees
            #endregion
            int direccionX = y < 0 ? 1 : -1; //comprueba si la y es positiva o negativa para que la direccion de x sea positiva o negativa
            float x = previousPOsition.x + direccionX * shootSpeed * corutineTime; //calculo de la nueva x que depende de la posicion anterior + la diferencia de x que tendr� en un tiempo dependiente de la velocida y el corutineTime
            Vector3 newPosition = new Vector3(x, y, 0); //vector 3 de la nueva posicion
            Vector3 direccion = (newPosition - previousPOsition).normalized; //vector director hacia la nueva posicion
            direccion = rotation * direccion; //rotamos el vector director usando el Quaternion rotation ---- IMPORTANTE EL ORDEN DE MULTIPLICAION ( quaternion * vector )
            m_Rigidbody.velocity = direccion * shootSpeed; //cambiamos la velocidad seg�n la direccion ya rotada y el shootSpeed
            yield return new WaitForSeconds(corutineTime); //esperamos corutinTime para reiniciar el bucle
            previousPOsition = newPosition; //ponemos como previous position la nueva antes de reiniciar el bucle
        }
        gameObject.SetActive(false); //una vez sale del bucle, la bala ya ha vuelto al origen, se desactiva
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    public void Init(int shotDamage, Vector3 playerTransform)
    {
        Vector3 direction = (playerTransform - transform.position).normalized;
        transform.right = direction;
        m_Rigidbody.velocity = direction * m_ShotSpeed;
        m_HitboxController.damage = shotDamage;
    }

    public void InitPLayer(int shotDamage, Transform transformShootPosition, Transform playerTransf, AdvanceShootsTypes.AdvanceShootsEnum tipus, float shootSpeed)
    {
        switch (tipus)
        {
            case AdvanceShootsTypes.AdvanceShootsEnum.NORMAL:
                this.transform.position = transformShootPosition.position;
                m_Rigidbody.velocity = playerTransf.right * shootSpeed;
                m_HitboxController.damage = shotDamage;
                break;
            case AdvanceShootsTypes.AdvanceShootsEnum.BOOMERANG:

                StartCoroutine(ellipseMovement(playerTransf, transformShootPosition, shotDamage, shootSpeed * 2)); //en este caso el shootspeed es *2 para que llegue m�s lejos el efecto boomerang
                break;
            default:
                this.transform.position = transformShootPosition.position;
                m_Rigidbody.velocity = playerTransf.right * shootSpeed;
                m_HitboxController.damage = shotDamage;
                break;
        }
    }

    public void InitBoss(int shotDamage, GameObject referenciaPlayer)
    {
        StartCoroutine(chaserShot(shotDamage, referenciaPlayer));
    }
}
