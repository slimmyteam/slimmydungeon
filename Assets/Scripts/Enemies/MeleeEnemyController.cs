using System.Collections;
using UnityEngine;

[RequireComponent(typeof(DamageManager))]
public class MeleeEnemyController : MonoBehaviour
{
    [Header("Componentes")]
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;
    private Animator m_Animator;
    [SerializeField]
    private HitboxController m_HitboxController;

    [Header("Parámetros del Melee Enemy")]
    [SerializeField]
    private SOEnemy m_EnemyStats;
    private enum SwitchMachineStates { NONE, IDLE, CHASE, HIT, HURT, DEAD };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    public SOEnemy EnemyStats { get => m_EnemyStats; set => m_EnemyStats = value; }
    [SerializeField]
    private bool m_Dead;
    [SerializeField]
    private bool m_Cooldown;
    [SerializeField]
    PartidaSO PartidaSO;

    [Header("Áreas de detección")]
    [SerializeField]
    private DetectionAreaController m_DetectionArea;
    [SerializeField]
    private DetectionAreaController m_AttackArea;

    [Header("Variables de control de áreas")]
    private bool heEntradoAreaDeteccion;
    [SerializeField]
    private GameEvent actualizarCoins;
    private bool heEntradoAreaAtaque;

    [Header("Referencias a objetos")]
    private int m_PlayerAttacksLayer;
    private Transform m_playerTransform;

    [Header("Scripts")]
    private DamageManager m_DamageManager;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoGameObjectInt m_EnemyDead;

    [Header("Corutinas")]
    private Coroutine m_cooldownCorutine;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//

    private void Awake()
    {
        m_Rigidbody = this.GetComponent<Rigidbody2D>();
        m_Animator = this.GetComponent<Animator>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_DamageManager = this.gameObject.GetComponent<DamageManager>();
        m_PlayerAttacksLayer = LayerMask.NameToLayer("PlayerAttacks");
    }

    public void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnEnable()
    {
        m_DetectionArea.OnDetected += PlayerDetected;
        m_DetectionArea.OnExit += PlayerOutOfDetected;
        m_AttackArea.OnDetected += PlayerInRange;
        m_AttackArea.OnExit += PlayerOutOfRange;

        m_Cooldown = false;
        m_DamageManager.OnFire = false;
        m_DamageManager.SO = m_EnemyStats;
        m_Dead = false;
        InitState(SwitchMachineStates.IDLE);
        m_SpriteRenderer.color = Color.white;
        heEntradoAreaDeteccion = false;
        heEntradoAreaAtaque = false;
    }

    private void OnDestroy()
    {
        m_DetectionArea.OnDetected -= PlayerDetected;
        m_DetectionArea.OnExit -= PlayerOutOfDetected;
        m_AttackArea.OnDetected -= PlayerInRange;
        m_AttackArea.OnExit -= PlayerOutOfRange;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerAttacksLayer)
        {
            m_Dead = m_DamageManager.LoseHealth(collision.gameObject.GetComponent<HitboxController>().damage, collision.gameObject.GetComponent<HitboxController>().type);
            if (m_Dead)
            {
                m_EnemyDead.Raise(collision.gameObject, m_EnemyStats.experience);
                ChangeState(SwitchMachineStates.DEAD);
            }
            else
                ChangeState(SwitchMachineStates.HURT);
        }
    }

    //----------------------------------[ MÁQUINA DE ESTADOS }----------------------------------//

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState) return;
        if (m_CurrentState == SwitchMachineStates.DEAD) return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemyIdle");
                break;

            case SwitchMachineStates.CHASE:
                m_Animator.Play("MeleeEnemyWalk");
                break;

            case SwitchMachineStates.HIT:
                break;

            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.red;
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemyHurt");
                break;

            case SwitchMachineStates.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                Die();
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.CHASE:
                m_Rigidbody.velocity = Vector3.zero;
                break;

            case SwitchMachineStates.HIT:
                m_Cooldown = false;
                if (m_cooldownCorutine != null) StopCoroutine(m_cooldownCorutine);
                break;

            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.white;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.CHASE:
                Vector2 direction = (m_playerTransform.position - this.transform.position).normalized;
                m_Rigidbody.velocity = direction * m_EnemyStats.speed;
                Encarar();
                break;

            case SwitchMachineStates.HIT:
                if (!m_Cooldown)
                {
                    m_Rigidbody.velocity = Vector2.zero;
                    m_HitboxController.damage = m_EnemyStats.damage;
                    m_HitboxController.type = m_EnemyStats.damageType;
                    m_cooldownCorutine = StartCoroutine(Cooldown());
                }
                break;

            case SwitchMachineStates.HURT:
                break;

            default:
                break;
        }
    }

    //----------------------------------[ DELEGADOS }----------------------------------//
    private void PlayerDetected(Transform playerTransform)
    {
        heEntradoAreaDeteccion = true;
        heEntradoAreaAtaque = false;
        m_playerTransform = playerTransform;
        ChangeState(SwitchMachineStates.CHASE);
    }

    private void PlayerOutOfDetected()
    {
        heEntradoAreaDeteccion = false;
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void PlayerInRange(Transform playerTransform)
    {
        heEntradoAreaAtaque = true;
        ChangeState(SwitchMachineStates.HIT);
    }

    private void PlayerOutOfRange()
    {
        heEntradoAreaAtaque = false;
        if (heEntradoAreaDeteccion)
        {
            ChangeState(SwitchMachineStates.CHASE);
        }
        else
        {
            ChangeState(SwitchMachineStates.IDLE);
        }
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator Cooldown()
    {
        m_Cooldown = true;
        m_Animator.Play("MeleeEnemyAttack", 0, 0f);
        yield return new WaitForSeconds(4);
        m_Cooldown = false;
    }

    //----------------------------------[ EVENTOS ANIMACIONES }----------------------------------//

    public void FinalizaHurt()
    {
        m_SpriteRenderer.color = Color.white;
        if (heEntradoAreaAtaque)
        {
            ChangeState(SwitchMachineStates.HIT);
        }
        else if (heEntradoAreaDeteccion)
        {
            ChangeState(SwitchMachineStates.CHASE);
        }
        else
        {
            ChangeState(SwitchMachineStates.IDLE);
        }

    }

    private void Die()
    {
        if (PartidaSO.money + EnemyStats.money <= 99999) PartidaSO.money += EnemyStats.money;
        else PartidaSO.money = 99999;
        actualizarCoins.Raise();
        m_DetectionArea.OnDetected -= PlayerDetected;
        m_DetectionArea.OnExit -= PlayerOutOfDetected;
        m_AttackArea.OnDetected -= PlayerInRange;
        m_AttackArea.OnExit -= PlayerOutOfRange;
        int r = Random.Range(0, m_EnemyStats.itemList.Length);
        GameObject item = Instantiate(m_EnemyStats.itemList[r]);
        item.transform.position = transform.position;
        this.gameObject.SetActive(false);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    private void Encarar()
    {
        this.transform.right = m_Rigidbody.velocity;
    }

    public void LlamarAMuerte()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }
}
