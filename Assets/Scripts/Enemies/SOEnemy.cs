using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Enemies")]
public class SOEnemy : ScriptableObject
{
    public int hp;
    public int maxHP;
    public int damage;
    public DamageTypes.DamageTypesEnum damageType;
    public float speed;
    public int money;
    public int experience;
    public int probablityOfDropItem;
    public GameObject[] itemList;
}
