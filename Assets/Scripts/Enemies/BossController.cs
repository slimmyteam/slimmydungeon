﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [Header("Componentes")]
    private SpriteRenderer m_SpriteRenderer;

    [Header("Parámetros del Boss")]
    [SerializeField]
    private bool m_Dead;
    private enum SwitchMachineStates { NONE, IDLE, FLEE, SHOOT, BURNED, HURT, DEAD };
    [SerializeField]
    private SwitchMachineStates m_EstadoActual;

    [Header("Scriptable Objects")]
    [SerializeField]
    private SOEnemy m_SOEnemy;
    public SOEnemy EnemyStats { get => m_SOEnemy; set => m_SOEnemy = value; }

    [Header("Disparo")]
    [SerializeField]
    private GameObject m_ShotPool;
    public GameObject ShootPool { get => m_ShotPool; set => m_ShotPool = value; }
    [SerializeField]
    private Transform m_ShotPosition;
    [SerializeField]
    private int m_ShotDamage;
    private GameObject disparo;

    [Header("Área boss")]
    [SerializeField]
    private BossAreaController m_AreaBoss;

    [Header("Referencias a objetos")]
    private List<GameObject> m_players = new List<GameObject>();

    [Header("Control de tiempos")]
    [SerializeField]
    private float m_TiempoEsperaDisparo;
    [SerializeField]
    private float m_TiempoEsperaDisparoArea;
    [SerializeField]
    private PartidaSO PartidaSO;
    private int m_PlayerAttacksLayer;

    [Header("Scripts")]
    private DamageManager m_DamageManager;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoGameObjectInt m_EnemyDead;
    [SerializeField]
    private GameEvent actualizarCoins;
    [SerializeField]
    private GameEvent puertaTrampa;
    [SerializeField]
    private GameEvent bossMorido;

    [Header("Corutinas")]
    private Coroutine m_disparando;
    private Coroutine m_esperaDisparo;
    private Coroutine m_reciboDano;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_DamageManager = this.gameObject.GetComponent<DamageManager>();

        m_ShotDamage = m_SOEnemy.damage;

        m_PlayerAttacksLayer = LayerMask.NameToLayer("PlayerAttacks");
    }

    private void OnEnable()
    {
        m_AreaBoss.OnDetected += Disparo;
        m_DamageManager.SO = m_SOEnemy;
        m_DamageManager.OnFire = false;
        m_Dead = false;
        IniciarEstado(SwitchMachineStates.IDLE);
        m_SpriteRenderer.color = Color.white;
    }

    void Start()
    {
        IniciarEstado(SwitchMachineStates.IDLE);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerAttacksLayer)
        {
            m_Dead = m_DamageManager.LoseHealth(collision.gameObject.GetComponent<HitboxController>().damage, collision.gameObject.GetComponent<HitboxController>().type);
            if (m_Dead)
            {
                m_EnemyDead.Raise(collision.gameObject, EnemyStats.experience);
                ChangeState(SwitchMachineStates.DEAD);
            }
            else
                ChangeState(SwitchMachineStates.HURT);
        }
    }

    private void OnDestroy()
    {
        m_AreaBoss.OnDetected -= Disparo;

        gameObject.SetActive(false);
    }

    //----------------------------------[ MÁQUINA DE ESTADOS }----------------------------------//

    private void ChangeState(SwitchMachineStates nuevoEstado)
    {
        if (nuevoEstado == m_EstadoActual) return;
        if (m_EstadoActual == SwitchMachineStates.DEAD) return;

        FinalizarEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(SwitchMachineStates nuevoEstado)
    {
        m_EstadoActual = nuevoEstado;
        switch (m_EstadoActual)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.SHOOT:
                m_esperaDisparo = StartCoroutine(EsperaDisparoArea());
                break;

            case SwitchMachineStates.BURNED:
                break;

            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.red;
                m_reciboDano = StartCoroutine(ReciboDano());
                break;

            case SwitchMachineStates.DEAD:
                Die();
                break;

            default:
                break;
        }
    }

    private void FinalizarEstado()
    {
        switch (m_EstadoActual)
        {
            case SwitchMachineStates.SHOOT:
                if (m_esperaDisparo != null)
                    StopCoroutine(m_esperaDisparo);
                if (m_disparando != null)
                    StopCoroutine(m_disparando);
                break;
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.HURT:
                m_SpriteRenderer.color = Color.white;
                break;
        }
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator Disparando()
    {
        while (true)
        {
            GameObject randomPlayer = m_players[Random.Range(0, m_players.Count)];
            Encarar(randomPlayer.transform.position - transform.position);
            disparo = m_ShotPool.gameObject.GetComponent<ShotsPoolController>().RequestShot();
            disparo.GetComponent<HitboxController>().damage = m_SOEnemy.damage;
            disparo.GetComponent<HitboxController>().type = m_SOEnemy.damageType;

            disparo.transform.position = m_ShotPosition.position;

            disparo.SetActive(true);
            disparo.GetComponent<ShotController>().InitBoss(m_ShotDamage, randomPlayer);
            yield return new WaitForSeconds(m_TiempoEsperaDisparo);
        }
    }

    private IEnumerator EsperaDisparoArea()
    {
        yield return new WaitForSeconds(m_TiempoEsperaDisparoArea);
        m_disparando = StartCoroutine(Disparando());

    }

    private IEnumerator ReciboDano()
    {
        yield return new WaitForSeconds(0.5f);
        FinalizaHurt();
    }

    //-------------------------------[ DELEGADOS AREAS DETECCION ]-------------------------------//

    public void Disparo(GameObject player)
    {
        if (!m_players.Contains(player))
        {
            m_players.Add(player);

            if (m_players.Count == 2)
            {
                m_AreaBoss.gameObject.SetActive(false);
                puertaTrampa.Raise();
            }

        }
        ChangeState(SwitchMachineStates.SHOOT);
    }

    //-----------------------------[ EVENTOS ANIMACIONES ]-------------------------------//
    public void FinalizaHurt()
    {
        StopCoroutine(m_reciboDano);
        m_SpriteRenderer.color = Color.white;
        ChangeState(SwitchMachineStates.SHOOT);
    }

    private void Die()
    {
        if (PartidaSO.money + EnemyStats.money <= 99999) PartidaSO.money += EnemyStats.money;
        else PartidaSO.money = 99999;
        actualizarCoins.Raise();
        m_AreaBoss.OnDetected -= Disparo;
        int r = Random.Range(0, EnemyStats.itemList.Length);
        GameObject item = Instantiate(EnemyStats.itemList[r]);
        item.transform.position = transform.position;
        bossMorido.Raise();
        gameObject.SetActive(false);
    }

    //-----------------------------[ FUNCIONES ]-------------------------------//

    private void Encarar(Vector3 direccion)
    {
        if (direccion.x < 0) //Si la x es menor a 0, rotamos
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            transform.eulerAngles = Vector3.zero;
        }
    }

    public void LlamarAMuerte()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }
}
