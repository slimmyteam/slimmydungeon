using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/ShotPools")]
public class SOShotPools : ScriptableObject
{
    [SerializeField]
    private GameObject shotPrefab;
    [SerializeField]
    private int poolSize;

    public GameObject ShotPrefab { get => shotPrefab; }
    public int PoolSize { get => poolSize; }
}
