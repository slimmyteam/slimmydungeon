using System.Collections.Generic;
using UnityEngine;
using static DamageTypes;

public class ShotsPoolController : MonoBehaviour
{
    [Header("Parámetros de la Shot Pool")]
    [SerializeField]
    private int poolSize;
    [SerializeField]
    private List<GameObject> shotList = new List<GameObject>();
    private DamageTypesEnum type;
    public DamageTypesEnum Type { get => type; set => type = value; }

    [Header("Scriptable objects")]
    [SerializeField]
    private SOShotPools m_SOShotPool;

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject shotPrefab;
    public GameObject ShotPrefab { get => shotPrefab; set => shotPrefab = value; }


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        shotPrefab = m_SOShotPool.ShotPrefab;
        poolSize = m_SOShotPool.PoolSize;
    }

    private void Start()
    {
        AddShotsToPool(poolSize);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    private void AddShotsToPool(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject shot = Instantiate(shotPrefab);
            shot.SetActive(false);
            shotList.Add(shot);
        }
    }

    public GameObject RequestShot()
    {
        for (int i = 0; i < shotList.Count; i++)
        {
            if (!shotList[i].activeSelf)
            {
                shotList[i].GetComponent<HitboxController>().type = type;
                shotList[i].SetActive(true);
                return shotList[i];
            }
        }
        AddShotsToPool(1);
        shotList[shotList.Count - 1].SetActive(true);
        return shotList[shotList.Count - 1];
    }
}
