using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/DATABASE")]
public class ObjectDataBase : ScriptableObject
{
    [SerializeField]
    private List<SOItems> _Items;

    public SOItems GetItemByID(int id) => _Items.FirstOrDefault(x => x.ID == id);
}
