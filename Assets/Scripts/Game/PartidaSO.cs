using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Partida")]
public class PartidaSO : ScriptableObject
{
    public int money;
    public int floor;

}
