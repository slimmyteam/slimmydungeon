using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Singleton")]
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;

    [Header("Referencias a objetos")]
    [SerializeField]
    private PartidaSO m_Partida;
    [SerializeField]
    private GameObject m_Player1;
    [SerializeField]
    private GameObject m_Player2;
    private PlayerSO m_Player1SO;
    private PlayerSO m_Player2SO;
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    [SerializeField]
    private GameObject m_Inventory;
    private InventorySO m_InventorySO;

    [Header("Guardar partida")]
    [SerializeField]
    private const string saveFileName = "Assets/Saves/savegame.json";
    private SaveData m_Data;

    [Header("Niveles")]
    [SerializeField]
    private string[] m_SceneNames;
    [SerializeField]
    private string m_EndGameSceneName;
    [SerializeField]
    private string m_GameOverSceneName;
    [SerializeField]
    private string m_MenuScene;

    [Header("Eventos")]
    [SerializeField]
    private GEGenericoString m_lanzarMensaje;
    [SerializeField]
    private GameEvent m_ReiniciarPlayers;

    [Header("Audios")]
    [SerializeField]
    private AudioClip[] m_listaAudioClips; //(temporalmente victory es 0 deber�a ser menu music), 1 es gameover, 2 es ambient music, 3 victory

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        BuscarPlayers();

        m_Input = Instantiate(m_InputActionAsset);
        m_Input.FindActionMap("Default").FindAction("Guardar").performed += SaveGame;
        m_Input.FindActionMap("Default").FindAction("Cargar").performed += LoadGame;
        m_Input.FindActionMap("Default").Enable();

        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[0];
        this.gameObject.GetComponent<AudioSource>().Play();
    }


    private void OnDestroy()
    {
        if (m_Input != null)
        {
            m_Input.FindActionMap("Default").FindAction("Guardar").performed -= SaveGame;
            m_Input.FindActionMap("Default").FindAction("Cargar").performed -= LoadGame;
            m_Input.FindActionMap("Default").Disable();
        }
    }

    //----------------------------------[ EVENTOS }----------------------------------//

    public void NewGame()
    {
        SceneManager.sceneLoaded += OnNewGame;
        SceneManager.LoadScene(m_SceneNames[0]);
    }

    private void OnNewGame(Scene arg0, LoadSceneMode arg1)
    {
        BuscarPlayers();

        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[1];
        this.gameObject.GetComponent<AudioSource>().Play();

        m_Partida.floor = 0;
        m_Partida.money = 0;
        m_InventorySO.ItemSlots.Clear();
        m_Player1.SetActive(true);
        m_Player2.SetActive(true);

        m_ReiniciarPlayers.Raise();

        SceneManager.sceneLoaded -= OnNewGame;
    }

    public void PlayerRevivido(GameObject player)
    {
        m_lanzarMensaje.Raise("El jugador " + player.name + " ha revivido");

        if (player.name == "Player") player.transform.position = new Vector2(m_Player2.transform.position.x - 0.3f, m_Player2.transform.position.y);
        else if (player.name == "Player 2") player.transform.position = new Vector2(m_Player1.transform.position.x - 0.3f, m_Player1.transform.position.y);
    }

    //----------------------------------[ INPUTS }----------------------------------//

    private void SaveGame(InputAction.CallbackContext actionContext)
    {
        if (m_Inventory == null)
        {
            m_Inventory = GameObject.Find("Inventory");
        }
        if (m_Player1 == null)
        {
            m_Player1 = GameObject.Find("Player");
        }

        if (m_Player2 == null)
        {
            m_Player2 = GameObject.Find("Player 2");
        }

        SaveData data = new SaveData();
        data.gameData = new SaveData.GameData(m_Partida, m_Player1SO, m_Player1.transform.position, m_Player2SO, m_Player2.transform.position, m_Player1SO.vida, m_Player2SO.vida, m_InventorySO, m_Player1SO.itemEquipado, m_Player2SO.itemEquipado, m_Player1SO.lvl, m_Player2SO.lvl);
        string jsonData = JsonUtility.ToJson(data);

        try
        {
            m_lanzarMensaje.Raise("Guardando partida ...");

            File.WriteAllText(saveFileName, jsonData);
        }
        catch (Exception e)
        {
            m_lanzarMensaje.Raise("Error al guardar la partida: "+e);
        }

    }

    public void LoadGame(InputAction.CallbackContext actionContext)
    {
        LoadGame();

    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    public void NextFloor()
    {
        if (m_SceneNames.Length <= m_Partida.floor + 1)
        {
            SceneManager.sceneLoaded += OnVictoryScene;
            SceneManager.LoadScene(m_EndGameSceneName);
        }
        else
        {
            print("next floor");
            m_Partida.floor += 1;

            SceneManager.sceneLoaded += OnLoadLevel;
            SceneManager.LoadScene(m_SceneNames[m_Partida.floor]);
        }
    }

    private void OnLoadLevel(Scene arg0, LoadSceneMode arg1)
    {
        BuscarPlayers();

        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[1];
        this.gameObject.GetComponent<AudioSource>().Play();

        SceneManager.sceneLoaded -= OnLoadLevel;
    }

    public void GameOver()
    {
        if (m_Player1SO.vida <= 0 && m_Player2SO.vida <= 0)
        {
            SceneManager.sceneLoaded += OnGameOver;
            SceneManager.LoadScene(m_GameOverSceneName);
        }
    }

    private void OnGameOver(Scene arg0, LoadSceneMode arg1)
    {
        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[2];
        this.gameObject.GetComponent<AudioSource>().Play();
        SceneManager.sceneLoaded -= OnGameOver;
    }

    public void Menu()
    {
        SceneManager.sceneLoaded += OnMenu;
        SceneManager.LoadScene(m_MenuScene);
    }

    private void OnMenu(Scene arg0, LoadSceneMode arg1)
    {
        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[0];
        this.gameObject.GetComponent<AudioSource>().Play();
        SceneManager.sceneLoaded -= OnMenu;
    }

    private void OnVictoryScene(Scene arg0, LoadSceneMode arg1)
    {
        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[3];
        this.gameObject.GetComponent<AudioSource>().Play();
        SceneManager.sceneLoaded -= OnVictoryScene;
    }

    private void BuscarPlayers()
    {
        if (m_Inventory == null)
            m_Inventory = GameObject.Find("Inventory");

        if (m_Player1 == null)
            m_Player1 = GameObject.Find("Player");

        if (m_Player2 == null)
            m_Player2 = GameObject.Find("Player 2");

        m_Player1SO = m_Player1.GetComponent<PlayerController>().SOPlayer;
        m_Player2SO = m_Player2.GetComponent<Player2Controller>().SOPlayer;
        m_InventorySO = m_Inventory.GetComponent<InventoryController>().inventorySO;
    }

    public void LoadGame()
    {
        try
        {
            m_lanzarMensaje.Raise("Cargando partida ...");

            string jsonData = File.ReadAllText(saveFileName);

            m_Data = new SaveData();
            JsonUtility.FromJsonOverwrite(jsonData, m_Data);

            //RESTAURAR PARTIDA
            m_Partida.money = m_Data.gameData.money;
            m_Partida.floor = m_Data.gameData.floor;

            SceneManager.sceneLoaded += OnLoadGame;
            SceneManager.LoadScene(m_SceneNames[m_Partida.floor]);//RESTAURAR PLAYERS

        }
        catch (Exception e)
        {
            m_lanzarMensaje.Raise("Error al cargar la partida: "+e);
        }

    }

    private void OnLoadGame(UnityEngine.SceneManagement.Scene arg0, LoadSceneMode arg1)
    {
        m_Inventory = GameObject.Find("Inventory");
        m_Player1 = GameObject.Find("Player");
        m_Player2 = GameObject.Find("Player 2");

        //Player 1 Datos
        float movVelocityP1 = m_Data.gameData.movVelocityP1;
        int dmgP1 = m_Data.gameData.dmgP1;
        DamageTypes.DamageTypesEnum dmgTypeP1 = m_Data.gameData.dmgTypeP1;
        int expP1 = m_Data.gameData.expP1;
        int expForNextLvlP1 = m_Data.gameData.expForNextLevelP1;
        int maxHPP1 = m_Data.gameData.maxHPP1;
        Vector3 positionPlayer1 = m_Data.gameData.positionPlayer1;
        int player1HP = m_Data.gameData.player1HP;
        SOItems itemEquipadoP1 = m_Data.gameData.itemEquipadoPlayer1;
        int lvlP1 = m_Data.gameData.lvlPlayer1;

        //Player 2 Datos
        float movVelocityP2 = m_Data.gameData.movVelocityP2;
        int dmgP2 = m_Data.gameData.dmgP2;
        DamageTypes.DamageTypesEnum dmgTypeP2 = m_Data.gameData.dmgTypeP2;
        int expP2 = m_Data.gameData.expP2;
        int expForNextLvlP2 = m_Data.gameData.expForNextLevelP2;
        int maxHPP2 = m_Data.gameData.maxHPP2;
        Vector3 positionPlayer2 = m_Data.gameData.positionPlayer2;
        int player2HP = m_Data.gameData.player2HP;
        SOItems itemEquipadoP2 = m_Data.gameData.itemEquipadoPlayer2;
        int lvlP2 = m_Data.gameData.lvlPlayer2;

        //Restaurar
        m_Player1.GetComponent<PlayerController>().RestaurarPlayer(movVelocityP1, dmgP1, dmgTypeP1, expP1, expForNextLvlP1, maxHPP1, positionPlayer1, player1HP, itemEquipadoP1, lvlP1);
        m_Player2.GetComponent<Player2Controller>().RestaurarPlayer(movVelocityP2, dmgP2, dmgTypeP2, expP2, expForNextLvlP2, maxHPP2, positionPlayer2, player2HP, itemEquipadoP2, lvlP2);

        //RESTAURAR INVENTARIO
        List<InventorySO.ItemSlot> itemSlots = m_Data.gameData.itemSlots;
        m_InventorySO.ItemSlots = itemSlots;

        this.gameObject.GetComponent<AudioSource>().clip = m_listaAudioClips[0];
        this.gameObject.GetComponent<AudioSource>().Play();

        SceneManager.sceneLoaded -= OnLoadGame;
    }
}
