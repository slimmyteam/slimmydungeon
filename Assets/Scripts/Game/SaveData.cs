using System;
using System.Collections.Generic;
using UnityEngine;
using static InventorySO;

[Serializable]
public class SaveData
{
    [Serializable]
    public struct GameData
    {
        [Header("Partida")]
        public int money;
        public int floor;

        [Header("Player 1")]
        public float movVelocityP1;
        public int dmgP1;
        public DamageTypes.DamageTypesEnum dmgTypeP1;
        public int expP1;
        public int expForNextLevelP1;
        public int maxHPP1;
        public Vector3 positionPlayer1;
        public int player1HP;
        public SOItems itemEquipadoPlayer1;
        public int lvlPlayer1;


        [Header("Player 2")]
        public float movVelocityP2;
        public int dmgP2;
        public DamageTypes.DamageTypesEnum dmgTypeP2;
        public int expP2;
        public int expForNextLevelP2;
        public int maxHPP2;
        public Vector3 positionPlayer2;
        public int player2HP;
        public SOItems itemEquipadoPlayer2;
        public int lvlPlayer2;

        [Header("Inventario")]
        public List<ItemSlot> itemSlots;

        public GameData(PartidaSO partida, PlayerSO player1SO, Vector3 positionPlayer1, PlayerSO player2SO, Vector3 positionPlayer2, int player1HP, int player2HP, InventorySO inventorySO, SOItems itemEquipadoP1, SOItems itemEquipadoP2, int lvlPlayer1, int lvlPlayer2)
        {
            money = partida.money;
            floor = partida.floor;

            movVelocityP1 = player1SO.MovVelocity;
            dmgP1 = player1SO.dmg;
            dmgTypeP1 = player1SO.damageType;
            expP1 = player1SO.exp;
            expForNextLevelP1 = player1SO.expForNextLevel;
            maxHPP1 = player1SO.vidaMax;
            this.positionPlayer1 = positionPlayer1;
            this.player1HP = player1HP;
            itemEquipadoPlayer1 = itemEquipadoP1;
            this.lvlPlayer1 = lvlPlayer1;


            movVelocityP2 = player2SO.MovVelocity;
            dmgP2 = player2SO.dmg;
            dmgTypeP2 = player2SO.damageType;
            expP2 = player2SO.exp;
            expForNextLevelP2 = player2SO.expForNextLevel;
            maxHPP2 = player2SO.vidaMax;
            this.positionPlayer2 = positionPlayer2;
            this.player2HP = player2HP;
            itemEquipadoPlayer2 = itemEquipadoP2;
            this.lvlPlayer2 = lvlPlayer2;

            itemSlots = new List<ItemSlot>();
            foreach (ItemSlot i in inventorySO.ItemSlots)
            {
                itemSlots.Add(i);
            }

        }
    }

    public GameData gameData;

}
