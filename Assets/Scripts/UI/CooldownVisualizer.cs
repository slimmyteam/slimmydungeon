using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CooldownVisualizer : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private Image fill;

    //----------------------------------[ EVENTOS }----------------------------------//
    public void MostrarCooldown(float tiempo)
    {
        StopAllCoroutines();
        StartCoroutine(cooldown(tiempo));
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator cooldown(float tiempo)
    {
        float currentTime = 0f;
        while (currentTime <= tiempo)
        {
            fill.fillAmount = Mathf.Lerp(1, 0, currentTime / tiempo);
            yield return null;
            currentTime += Time.deltaTime;
        }
        fill.fillAmount = 0;
    }
}
