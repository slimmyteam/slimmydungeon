using UnityEngine;

public class ButtonController : MonoBehaviour
{
    [Header("Eventos")]
    [SerializeField]
    private GameEvent gameEvent;

    //----------------------------------[ EVENTOS }----------------------------------//
    public void BotonPulsado()
    {
        gameEvent.Raise();
    }
}
