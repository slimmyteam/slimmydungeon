using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaController : MonoBehaviour
{
    [Header("Parámetros Barra Vida")]
    [SerializeField]
    private Gradient m_gradient;
    private float m_maxHp;
    private float m_currentHp;
    private float m_lastHp;
    [SerializeField]
    private float m_lerpTime;

    [Header("Scriptable objects")]
    [SerializeField]
    private PlayerSO m_SOPlayer;

    [Header("Refernencias a objetos")]
    [SerializeField]
    private Image m_fill;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    void Start()
    {
        m_maxHp = m_SOPlayer.vidaMax;
        m_currentHp = m_SOPlayer.vida;
        m_fill.fillAmount = m_currentHp / m_maxHp;
        m_fill.color = m_gradient.Evaluate(m_fill.fillAmount);

    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator ActualitzarVida()
    {
        float currentTime = 0f;
        while (currentTime <= m_lerpTime)
        {
            m_fill.fillAmount = Mathf.Lerp(m_lastHp / m_maxHp, m_currentHp / m_maxHp, currentTime / m_lerpTime);
            m_fill.color = m_gradient.Evaluate(m_fill.fillAmount);
            yield return null;
            currentTime += Time.deltaTime;
        }

    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    public void perderVida()
    {
        StopAllCoroutines();
        m_lastHp = m_currentHp;
        m_currentHp = m_SOPlayer.vida;
        StartCoroutine(ActualitzarVida());
    }

}
