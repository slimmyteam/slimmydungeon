using UnityEngine;
using UnityEngine.UI;

public class EquipedItemVisualizer : MonoBehaviour
{

    //----------------------------------[ EVENTOS }----------------------------------//
    public void ActualizarImagen(SOItems item)
    {
        if (item != null) this.gameObject.GetComponent<Image>().sprite = item.Sprite;
    }
}
