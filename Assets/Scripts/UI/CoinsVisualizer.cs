using TMPro;
using UnityEngine;

public class CoinsVisualizer : MonoBehaviour
{
    [Header("Scriptable objects")]
    [SerializeField]
    PartidaSO PartidaSO;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    void Start()
    {
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Coins:\n" + PartidaSO.money;
    }

    //----------------------------------[ EVENTOS }----------------------------------//
    public void ActualizarCoins()
    {
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Coins:\n" + PartidaSO.money;
    }
}
