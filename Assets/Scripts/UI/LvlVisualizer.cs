using TMPro;
using UnityEngine;

public class LvlVisualizer : MonoBehaviour
{
    //----------------------------------[ EVENTOS }----------------------------------//
    public void ActualizarLvl(int lvl)
    {
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "LVL: " + lvl;
    }
}
