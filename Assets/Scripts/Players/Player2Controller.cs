using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CameraBound))]
[RequireComponent(typeof(DamageManager))]
[RequireComponent(typeof(ExperienceManager))]
[RequireComponent(typeof(RestaurarPlayer))]
public class Player2Controller : MonoBehaviour, IUsable, IEquipable
{
    [Header("Componentes")]
    private Animator m_Animator;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_SpriteRenderer;

    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    [SerializeField]
    public InputActionAsset InputPlayer2 { get { return m_Input; } }
    private InputAction m_MovementAction;

    [Header("Parámetros del Player 2")]
    [SerializeField]
    private bool m_Invincible;
    [SerializeField]
    private bool m_Dead;
    [SerializeField]
    private SOItems m_ItemEquipable;
    private enum Mira { Der, Izq }; //enum que nos indica donde mira el personaje
    private Mira m_Mira;
    private enum SwitchMachineStates { NONE, IDLE, WALK, SHOOT, PUPA, DEAD, INVENTORY }; //enum de los estados de la maquina de estados
    [SerializeField]
    private SwitchMachineStates m_CurrentState; //estado actual de la maquina

    [Header("Scriptable objects")]
    [SerializeField]
    private PlayerSO m_SOPlayer;
    public PlayerSO SOPlayer { get { return m_SOPlayer; } set { m_SOPlayer = value; } }

    [Header("Disparo")]
    [SerializeField]
    private GameObject m_ShotPoolPlayer;
    [SerializeField]
    private Transform m_ShotPosition;
    [SerializeField]
    private float m_ShotSpeed = 5f;
    [SerializeField]
    private GameObject m_Disparo;
    [SerializeField]
    private GameObject m_Weapon;
    [SerializeField]
    private float m_CooldownTime;
    [SerializeField]
    private bool m_onCooldown;
    private AdvanceShootsTypes.AdvanceShootsEnum m_advanceShootsEnum;

    [Header("Eventos")]
    [SerializeField]
    private GEItem heCogidoObjeto;
    [SerializeField]
    private GEItem heCogidoObjetoUI;
    [SerializeField]
    private GameEvent actualizaDataPlayers;
    [SerializeField]
    private GEGenericoGameObject heRevivido;
    [SerializeField]
    private GEGenericoFloat activoCooldownUI;
    [SerializeField]
    private GameEvent trompetitaDeFarmeo;
    [SerializeField]
    private GameEvent abrirInventario;
    [SerializeField]
    private GameEvent abrirTienda;
    [SerializeField]
    private GameEvent playerMuerto;

    [Header("Scripts")]
    [SerializeField]
    private CameraController m_CameraController;
    private DamageManager m_DamageManager;
    private CameraBound m_CameraBound;
    private ExperienceManager m_ExperienceManager;
    private RestaurarPlayer m_RestaurarPlayer;

    [Header("Stats al iniciar partida")]
    [SerializeField]
    private int maxHpInicial;
    [SerializeField]
    private int velInicial;
    [SerializeField]
    private int dmgInicial;
    [SerializeField]
    private DamageTypes.DamageTypesEnum dmgTypeInicial;
    [SerializeField]
    private int expForNextLevelInicial;
    [SerializeField]
    private SOItems initialEquippedItem;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_DamageManager = GetComponent<DamageManager>();
        m_CameraBound = GetComponent<CameraBound>();
        m_ExperienceManager = GetComponent<ExperienceManager>();
        m_Animator = GetComponent<Animator>();
        m_RestaurarPlayer = GetComponent<RestaurarPlayer>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();

        m_Input = Instantiate(m_InputActionAsset); //Instanciem el asset (pf)
        m_Input.bindingMask = InputBinding.MaskByGroup("Player2");
    }

    private void OnEnable()
    {
        m_MovementAction = m_Input.FindActionMap("AMDefault").FindAction("Movement"); 
        m_Input.FindActionMap("AMDefault").FindAction("Attack").performed += Attack;
        m_Input.FindActionMap("AMDefault").FindAction("Trompeta").performed += Trompeta;
        m_Input.FindActionMap("AMDefault").FindAction("Inventory").performed += OnInventory;
        m_Input.FindActionMap("AMDefault").FindAction("Interact").performed += OnInteraction;
        m_Input.FindActionMap("AMDefault").Enable(); //Activem el ActionMap

        m_Dead = false;
        m_DamageManager.OnFire = false;
        m_DamageManager.SO = m_SOPlayer;
        m_ExperienceManager.SOPlayer = m_SOPlayer;
        m_ExperienceManager.Weapon = m_Weapon;
        m_RestaurarPlayer.PlayerSO = m_SOPlayer;
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnDisable()
    {
        if (m_Input != null)
        {
            m_Input.FindActionMap("AMDefault").FindAction("Trompeta").performed -= Trompeta;
            m_Input.FindActionMap("AMDefault").FindAction("Attack").performed -= Attack;
            m_Input.FindActionMap("AMDefault").FindAction("Inventory").performed -= OnInventory;
            m_Input.FindActionMap("AMDefault").FindAction("Inventory").performed -= OnInteraction;
            m_Input.FindActionMap("AMDefault").Disable(); //Desactivem el ActionMap
            m_CurrentState = SwitchMachineStates.NONE;
        }
    }

    void Start()
    {
        InitState(SwitchMachineStates.IDLE);
        m_advanceShootsEnum = AdvanceShootsTypes.AdvanceShootsEnum.NORMAL;
    }

    void Update()
    {
        UpdateState();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("EnemyAttacks") && !m_Invincible)
        {
            m_Dead = m_DamageManager.LoseHealth(collision.gameObject.GetComponent<HitboxController>().damage, collision.gameObject.GetComponent<HitboxController>().type);
            if (m_Dead)
                ChangeState(SwitchMachineStates.DEAD);
            else
            {
                ChangeState(SwitchMachineStates.PUPA);
                actualizaDataPlayers.Raise();
            }
        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            heCogidoObjeto.Raise(collision.gameObject.GetComponent<ItemController>().ItemSO); //he cogido un objeto y se lanza un evento de que lo he cogido (falta definir donde llega el evento???)
            Destroy(collision.gameObject);
        }
    }

    //----------------------------------[ MÁQUINA DE ESTADOS }----------------------------------//
    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                switch (m_Mira)
                {
                    case Mira.Der:
                        this.transform.right = Vector3.right;
                        break;

                    case Mira.Izq:
                        this.transform.right = -Vector3.right;
                        break;

                    default:
                        break;
                }

                break;

            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.SHOOT:
                if (m_onCooldown)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                    return;
                }

                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                m_Animator.Play("Player2Hit", 0, 0f);
                Disparar();
                activoCooldownUI.Raise(m_CooldownTime);
                StartCoroutine(EsperaAtaque());
                break;

            case SwitchMachineStates.PUPA:
                m_Animator.Play("Hurt2");
                break;

            case SwitchMachineStates.DEAD:
                m_Rigidbody.velocity = Vector2.zero;
                this.gameObject.SetActive(false);
                playerMuerto.Raise();
                break;

            case SwitchMachineStates.INVENTORY:
                m_Input.FindActionMap("AMDefault").Disable();
                m_Input.FindActionMap("Inventory").Enable();
                break;
            default:
                break;
        }
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState) return; //Evitamos que un estado entre a sí mismo
        if (m_CurrentState == SwitchMachineStates.DEAD) return;
        if (newState == SwitchMachineStates.PUPA && m_CurrentState == SwitchMachineStates.INVENTORY)
        {
            m_Animator.Play("Hurt2");
            m_SpriteRenderer.color = Color.red;
            return;
        }
        ExitState();
        InitState(newState);
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().x != 0 || m_MovementAction.ReadValue<Vector2>().y != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:
                if (m_CameraController.CameraBounds.Contains(this.transform.position))
                {
                    m_Rigidbody.velocity = m_MovementAction.ReadValue<Vector2>() * m_SOPlayer.MovVelocity;
                    this.transform.right = m_Rigidbody.velocity;
                }
                else
                {
                    m_CameraBound.MantenerseDentro(this.gameObject);

                }
                if (m_Rigidbody.velocity.x < 0)
                {
                    m_Mira = Mira.Izq;

                }
                else if (m_Rigidbody.velocity.x > 0)
                {
                    m_Mira = Mira.Der;
                }
                if (m_Rigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;
            case SwitchMachineStates.SHOOT:
                break;

            case SwitchMachineStates.PUPA:
                m_SpriteRenderer.color = Color.red;
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.SHOOT:
                break;

            case SwitchMachineStates.INVENTORY:
                m_Input.FindActionMap("Inventory").Disable();
                m_Input.FindActionMap("AMDefault").Enable();
                break;

            default:
                break;
        }
    }


    //----------------------------------[ INPUTS }----------------------------------//
    private void Attack(InputAction.CallbackContext actionContext)
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.SHOOT);
                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.SHOOT);
                break;

            case SwitchMachineStates.SHOOT:

                ChangeState(SwitchMachineStates.SHOOT);
                break;

            case SwitchMachineStates.PUPA:
                break;

            default:
                break;
        }
    }

    //----------------------------------[ EVENTOS }----------------------------------//

    private void Trompeta(InputAction.CallbackContext actionContext)
    {
        trompetitaDeFarmeo.Raise();
    }

    public void Init()
    {
        m_RestaurarPlayer.Restaurar(velInicial, dmgInicial, dmgTypeInicial, 0, expForNextLevelInicial, maxHpInicial, maxHpInicial, initialEquippedItem, 1);
    }
    private void OnInteraction(InputAction.CallbackContext actionContext)
    {
        abrirTienda.Raise();
    }

    private void OnInventory(InputAction.CallbackContext actionContext)
    {
        abrirInventario.Raise();
    }

    public void OnInventoryExit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    public void CambiaEstadoInventory()
    {
        ChangeState(SwitchMachineStates.INVENTORY);
    }

    //----------------------------------[ EVENTOS ANIMACIONES }----------------------------------//
    public void EndHit()
    {
        m_Invincible = false;
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void TakesDamage()
    {
        m_Invincible = true;
    }

    public void FinishHurt()
    {
        m_SpriteRenderer.color = Color.white;
        m_Invincible = false;
        if (m_CurrentState != SwitchMachineStates.INVENTORY) ChangeState(SwitchMachineStates.IDLE);
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator StatDuration(string stat, float oldValue, int seg)
    {
        yield return new WaitForSeconds(seg);
        switch (stat)
        {
            case "velocity":
                m_SOPlayer.MovVelocity = oldValue;
                break;
            case "shootSpeed":
                m_ShotSpeed = oldValue;
                break;
            default:
                break;
        }
    }

    private IEnumerator EsperaAtaque()
    {
        m_onCooldown = true;
        yield return new WaitForSeconds(m_CooldownTime);
        m_onCooldown = false;
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    private void Disparar()
    {
        m_Disparo = m_ShotPoolPlayer.gameObject.GetComponent<ShotsPoolController>().RequestShot();

        m_Disparo.GetComponent<ShotController>().InitPLayer(m_SOPlayer.dmg, m_ShotPosition, this.transform, m_advanceShootsEnum, m_ShotSpeed);
        m_Disparo.SetActive(true);
    }

    public void Heal(int healAmount, bool revive)
    {
        print("vida antes del heal item: " + m_SOPlayer.vida);
        if ((this.gameObject.activeSelf && !revive) || (!this.gameObject.activeSelf && revive))
        {
            if (m_SOPlayer.vida + healAmount <= SOPlayer.vidaMax)
            {
                m_SOPlayer.vida += healAmount;
            }
            else m_SOPlayer.vida = SOPlayer.vidaMax;
            m_DamageManager.curar();
        }
        if (revive)
        {
            this.gameObject.SetActive(true);
            ChangeState(SwitchMachineStates.INVENTORY);
            heRevivido.Raise(gameObject);
        }
        actualizaDataPlayers.Raise();
        print(m_SOPlayer.vida);
    }

    public void StatBuff(string statName, float buffAmount)
    {
        switch (statName)
        {
            case "velocity":
                m_SOPlayer.MovVelocity = m_SOPlayer.MovVelocity * buffAmount;
                break;
            case "shootSpeed":
                m_ShotSpeed = m_ShotSpeed * buffAmount;
                break;
            default:
                break;
        }
    }

    public void TempStatBuff(string statName, float buffAmount, int durationSec)
    {
        float oldValue = 1;
        switch (statName)
        {
            case "velocity":
                oldValue = m_SOPlayer.MovVelocity;
                m_SOPlayer.MovVelocity = m_SOPlayer.MovVelocity * buffAmount;
                break;
            case "shootSpeed":
                oldValue = m_ShotSpeed;
                m_ShotSpeed = m_ShotSpeed * buffAmount;
                break;
            default:
                break;
        }
        StartCoroutine(StatDuration(statName, oldValue, durationSec));
    }

    public void PowerRock(DamageTypes.DamageTypesEnum tipoDano, SOItems item)
    {
        SOItems viejoEquipable = m_ItemEquipable;
        m_ItemEquipable = item;
        m_SOPlayer.itemEquipado = item;
        m_advanceShootsEnum = AdvanceShootsTypes.AdvanceShootsEnum.NORMAL;
        m_ShotPoolPlayer.gameObject.GetComponent<ShotsPoolController>().Type = tipoDano;
        m_SOPlayer.damageType = tipoDano;
        if (viejoEquipable != null) heCogidoObjeto.Raise(viejoEquipable);
        heCogidoObjetoUI.Raise(m_ItemEquipable);
    }

    public void AdvanceShoot(AdvanceShootsTypes.AdvanceShootsEnum tipoDisp, SOItems item)
    {
        SOItems viejoEquipable = m_ItemEquipable;
        m_ItemEquipable = item;
        m_SOPlayer.itemEquipado = item;
        m_advanceShootsEnum = tipoDisp;
        if (viejoEquipable != null) heCogidoObjeto.Raise(viejoEquipable);
        heCogidoObjetoUI.Raise(m_ItemEquipable);
    }

    public void RestaurarPlayer(float movVelocity, int dmg, DamageTypes.DamageTypesEnum dmgType, int exp, int expForNextLevel, int maxHp, Vector3 position, int hp, SOItems itemEquipado, int lvl)
    {
        this.transform.position = position;
        m_RestaurarPlayer.Restaurar(movVelocity, dmg, dmgType, exp, expForNextLevel, maxHp, hp, itemEquipado, lvl);
    }

    public void LlamarAMuerte()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }
}
