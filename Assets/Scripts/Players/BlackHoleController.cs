using System.Collections;
using UnityEngine;

public class BlackHoleController : MonoBehaviour
{
    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void OnEnable()
    {
        StartCoroutine(girar());
        StartCoroutine(colapsar());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator girar()
    {
        while (true)
        {
            this.transform.Rotate(0, 0, 1);
            yield return new WaitForSeconds(0.05f);
        }
    }

    private IEnumerator colapsar()
    {
        yield return new WaitForSeconds(10);
        this.gameObject.SetActive(false);
    }
}
