using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Player")]
public class PlayerSO : ScriptableObject
{
    public int vida; //vida del personaje
    public float MovVelocity; // multipicador de velocidad de movimiento de personaje
    public int dmg;
    public DamageTypes.DamageTypesEnum damageType;
    public int exp;
    public int expForNextLevel;
    public int vidaMax;
    public int lvl;
    public SOItems itemEquipado;
}
