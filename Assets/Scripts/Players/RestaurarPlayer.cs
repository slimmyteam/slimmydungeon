using UnityEngine;

public class RestaurarPlayer : MonoBehaviour
{
    [Header("Eventos")]
    [SerializeField]
    private GEItem m_ItemEquipadoActualizado;

    [Header("Scriptable objects")]
    private PlayerSO m_PlayerSO;
    public PlayerSO PlayerSO { get { return m_PlayerSO; } set { m_PlayerSO = value; } }


    //----------------------------------[ FUNCIONES }----------------------------------//
    public void Restaurar(float movVelocity, int dmg, DamageTypes.DamageTypesEnum dmgType, int exp, int expForNextLevel, int maxHp, int hp, SOItems itemEquipado, int lvl)
    {
        print("restarurar " + m_PlayerSO);
        m_PlayerSO.MovVelocity = movVelocity;
        m_PlayerSO.dmg = dmg;
        m_PlayerSO.damageType = dmgType;
        m_PlayerSO.exp = exp;
        m_PlayerSO.expForNextLevel = expForNextLevel;
        m_PlayerSO.vidaMax = maxHp;
        m_PlayerSO.vida = hp;
        m_PlayerSO.itemEquipado = itemEquipado;
        m_PlayerSO.lvl = lvl;

        m_ItemEquipadoActualizado.Raise(itemEquipado);
    }

}
