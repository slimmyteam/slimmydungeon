using System.Collections.Generic;
using UnityEngine;

public class BlackHolePoolController : MonoBehaviour
{
    [Header("Singleton")]
    private static BlackHolePoolController m_Instance;
    public static BlackHolePoolController Instance => m_Instance;

    [Header("Parámetros Black Hole Pool")]
    [SerializeField]
    private int poolSize;
    [SerializeField]
    private List<GameObject> blackHoleList = new List<GameObject>();

    [Header("Scriptable objects")]
    [SerializeField]
    private SOShotPools m_SOShotPool;

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject blackHolePrefab;
    public GameObject BlackHolePrefab { get => blackHolePrefab; set => blackHolePrefab = value; }


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        blackHolePrefab = m_SOShotPool.ShotPrefab;
        poolSize = m_SOShotPool.PoolSize;
    }
    void Start()
    {
        AddShotsToPool(poolSize);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    private void AddShotsToPool(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject shot = Instantiate(blackHolePrefab);
            shot.transform.parent = this.transform;
            shot.SetActive(false);
            blackHoleList.Add(shot);
        }
    }

    public GameObject RequestBlackHole()
    {
        for (int i = 0; i < blackHoleList.Count; i++)
        {
            if (!blackHoleList[i].activeSelf)
            {
                blackHoleList[i].SetActive(true);
                return blackHoleList[i];
            }
        }
        return null;
    }
}
