using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class ExperienceManager : MonoBehaviour
{
    [Header("Par�metros Experience Manager")]
    private GameObject m_Weapon; //Arma que usa el player para matar
    public GameObject Weapon { get => m_Weapon; set => m_Weapon = value; }
    public GEGenericoInt eventoLvlUP;

    [Header("Scriptable objects")]
    private PlayerSO m_SOPlayer;

    [Header("Incrementos al subir de nivel")]
    [SerializeField]
    private int expIncrement; //Numero en que incrementa la experiencia necesaria para subir otro nivel
    public PlayerSO SOPlayer { get => m_SOPlayer; set => m_SOPlayer = value; }
    [SerializeField]
    private int hpIncrement;
    [SerializeField]
    private int dmgIncrement;

    [Header("VFX")]
    [SerializeField]
    private VisualEffect m_levelUpVFX;

    [Header("Corutinas")]
    private Coroutine m_particulasVFX;
    [SerializeField]
    private float esperaParticulasVFX;

    [Header("Audio")]
    private AudioSource[] m_audios;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_levelUpVFX.Stop();
        m_audios = GetComponents<AudioSource>();
    }

    private void OnEnable()
    {
        m_levelUpVFX.Stop();
    }

    private void Start()
    {
        eventoLvlUP.Raise(m_SOPlayer.lvl);
    }

    private void OnDestroy()
    {
        if (m_particulasVFX != null)
            StopCoroutine(m_particulasVFX);
    }

    //----------------------------------[ CORUTINAS }----------------------------------//

    private IEnumerator levelUpVFX()
    {
        m_levelUpVFX.Play();
        yield return new WaitForSeconds(esperaParticulasVFX);
        m_levelUpVFX.Stop();
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    public void GainExperience(GameObject weapon, int exp)
    {
        if (weapon.tag == m_Weapon.tag)
        {
            m_SOPlayer.exp += exp;
            if (m_SOPlayer.exp >= m_SOPlayer.expForNextLevel)
                LevelUpPlayer();
        }
    }
    public void LevelUpPlayer()
    {
        m_SOPlayer.vida += hpIncrement;
        m_SOPlayer.vidaMax += hpIncrement;
        m_SOPlayer.dmg += dmgIncrement;
        m_SOPlayer.lvl++;
        eventoLvlUP.Raise(m_SOPlayer.lvl);
        m_particulasVFX = StartCoroutine(levelUpVFX());
        m_audios[0].Play(); //level up sound
        print("level up");
        ResetExp();
    }

    public void ResetExp()
    {
        m_SOPlayer.exp = 0;
        m_SOPlayer.expForNextLevel += expIncrement;
    }
}
