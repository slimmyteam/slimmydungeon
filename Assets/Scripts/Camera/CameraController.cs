using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Componentes")]
    Camera m_Camera;

    [Header("Par�metros c�mara")]
    [SerializeField]
    private Vector3 m_CameraPosition;
    private static CameraController m_Instance;
    public static CameraController Instance => m_Instance;
    [SerializeField]
    private Bounds m_CameraBounds = new();
    public Bounds CameraBounds => m_CameraBounds;

    [Header("Referencias a objetos")]
    [SerializeField]
    private Transform m_Player1Transform;
    [SerializeField]
    private Transform m_Player2Transform;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_CameraPosition = this.transform.position;
        m_Camera = this.GetComponent<Camera>();
    }

    void LateUpdate()
    {
        //Si los dos players estan vivos
        if (m_Player1Transform.gameObject.activeInHierarchy && m_Player2Transform.gameObject.activeInHierarchy)
        {
            m_CameraPosition = new Vector3((m_Player1Transform.position.x + m_Player2Transform.position.x) / 2, (m_Player1Transform.position.y + m_Player2Transform.position.y) / 2, this.transform.position.z);
            this.transform.position = m_CameraPosition;
        }
        //Si solo el player1 est� vivo
        else if (m_Player1Transform.gameObject.activeInHierarchy)
        {
            Position1Player(m_Player1Transform);
        }
        //Si solo el player2 est� vivo
        else if (m_Player2Transform.gameObject.activeInHierarchy)
        {
            Position1Player(m_Player2Transform);
        }
        else
        {
            m_CameraPosition = new Vector3(0, 0, this.transform.position.z);
            this.transform.position = m_CameraPosition;
        }
        CalculateBounds();
    }


    //----------------------------------[ FUNCIONES }----------------------------------//
    private void Position1Player(Transform player)
    {
        m_CameraPosition = new Vector3(player.position.x, player.position.y, this.transform.position.z);
        this.transform.position = m_CameraPosition;
    }

    private void CalculateBounds()
    {
        m_CameraBounds.center = m_CameraPosition;
        m_CameraBounds.min = m_Camera.ViewportToWorldPoint(new Vector3(0.02f, 0.05f, -15));
        m_CameraBounds.max = m_Camera.ViewportToWorldPoint(new Vector3(0.98f, 0.95f, 15));
    }
}
