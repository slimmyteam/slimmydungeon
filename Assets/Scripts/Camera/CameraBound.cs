using UnityEngine;

public class CameraBound : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private CameraController m_CameraController;

    //----------------------------------[ FUNCIONES }----------------------------------//
    public void MantenerseDentro(GameObject player)
    {

        if (player.transform.position.x <= m_CameraController.CameraBounds.min.x)
        {
            player.transform.position = new Vector3(m_CameraController.CameraBounds.min.x, this.transform.position.y);
        }
        if (player.transform.position.x >= m_CameraController.CameraBounds.max.x)
        {
            player.transform.position = new Vector3(m_CameraController.CameraBounds.max.x, this.transform.position.y);
        }
        if (player.transform.position.y <= m_CameraController.CameraBounds.min.y)
        {
            player.transform.position = new Vector3(this.transform.position.x, m_CameraController.CameraBounds.min.y);
        }
        if (player.transform.position.y >= m_CameraController.CameraBounds.max.y)
        {
            player.transform.position = new Vector3(this.transform.position.x, m_CameraController.CameraBounds.max.y);
        }
    }
}
