using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessageVisualizer : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    GameObject m_MessageContainer;
    [SerializeField]
    TextMeshProUGUI m_Text;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    void Start()
    {
        m_MessageContainer.SetActive(false);
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator mostrar(string text)
    {
        m_Text.text = text;
        m_MessageContainer.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        m_MessageContainer.SetActive(false);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    public void MostrarMensaje(string text)
    {
        StartCoroutine(mostrar(text));
    }
}
