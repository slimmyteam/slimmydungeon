using System.Collections;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject[] m_textGameOver;
    [SerializeField]
    private GameObject m_buttonStart;
    [SerializeField]
    private GameObject m_buttonLoad;

    [Header("Gesti�n de tiempos")]
    [SerializeField]
    private int m_esperaDesactivacionGameOver;

    [Header("Audios")]
    private AudioSource[] m_audios;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_esperaDesactivacionGameOver = 4;
        MostrarTextoGameOver(false);
        MostrarBotones(false);
        m_audios = GetComponents<AudioSource>();
    }

    private void Start()
    {
        MostrarTextoGameOver(true);
        StartCoroutine(DesactivacionGameOver());
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator DesactivacionGameOver()
    {
        yield return new WaitForSeconds(m_esperaDesactivacionGameOver);
        MostrarTextoGameOver(false);
        MostrarBotones(true);
        m_audios[0].Play(); //slime sound
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    private void MostrarTextoGameOver(bool activo)
    {
        foreach (GameObject letra in m_textGameOver)
        {
            letra.gameObject.SetActive(activo);
        }
    }

    private void MostrarBotones(bool activo)
    {
        m_buttonStart.gameObject.SetActive(activo);
        m_buttonLoad.gameObject.SetActive(activo);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
