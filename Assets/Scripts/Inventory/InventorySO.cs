using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "Scriptable Objects/Inventory")]
public class InventorySO : ScriptableObject
{
    [SerializeField]
    private List<ItemSlot> m_ItemSlots = new List<ItemSlot>();
    public List<ItemSlot> ItemSlots { get { return m_ItemSlots; } set { m_ItemSlots = value; } }

    [Serializable] //Esto hace que lo podamos ver por el inspector
    public class ItemSlot
    {
        [SerializeField]
        public SOItems item;
        [SerializeField]
        public int amount;

        //Constructor
        public ItemSlot(SOItems soItem)
        {
            item = soItem;
            amount = 1;
        }
    }

    private ItemSlot GetItem(SOItems item)
    {
        //FirstOrDefault -->> Devuelve el primer elemento de una secuencia o un valor predeterminado si no se encuentra ning�n elemento.
        return m_ItemSlots.FirstOrDefault(slot => slot.item == item);
    }

    public void AddItem(SOItems itemToAdd)
    {
        ItemSlot item = GetItem(itemToAdd);

        if (item == null)
            m_ItemSlots.Add(new ItemSlot(itemToAdd));
        else
            item.amount++;

    }

    public void RemoveItem(SOItems itemUsado)
    {
        ItemSlot item = GetItem(itemUsado);

        if (item == null)
            return;

        item.amount--;
        if (item.amount <= 0)
            m_ItemSlots.Remove(item);
    }

}
