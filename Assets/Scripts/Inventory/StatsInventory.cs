using TMPro;
using UnityEngine;

public class StatsInventory : MonoBehaviour
{
    [Header("Scriptable objects")]
    [SerializeField]
    private PlayerSO playerSO;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void OnEnable()
    {
        GetComponent<TextMeshProUGUI>().text = "HP " + playerSO.vida + "/" + playerSO.vidaMax + "\r\nXP " + playerSO.exp + "/" + playerSO.expForNextLevel + "\r\nSpeed " + playerSO.MovVelocity + "\r\nStrength " + playerSO.dmg;
    }

    //----------------------------------[ EVENTOS }----------------------------------//
    public void actualizarData()
    {
        GetComponent<TextMeshProUGUI>().text = "HP " + playerSO.vida + "/" + playerSO.vidaMax + "\r\nXP " + playerSO.exp + "/" + playerSO.expForNextLevel + "\r\nSpeed " + playerSO.MovVelocity + "\r\nStrength " + playerSO.dmg;
    }
}
