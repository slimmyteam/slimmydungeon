using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class InventoryController : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;

    [Header("Scriptable Objects")]
    [SerializeField]
    private InventorySO m_inventarioSO;
    public InventorySO inventorySO { get { return m_inventarioSO; } set { m_inventarioSO = value; } }

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_inventarioContent;
    [SerializeField]
    private GameObject m_inventorySlotPrefab;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_actualizarInventario;
    [SerializeField]
    private GameEvent inventarioCerrado;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Input = Instantiate(m_InputActionAsset);
        m_Input.FindActionMap("Inventory").FindAction("Exit").performed += CerrarInventario;
    }

    private void Start()
    {
        RellenarInventario();
    }

    //-------------------------------[ EVENTOS ]-------------------------------//
    public void AddItem(SOItems item)
    {
        m_inventarioSO.AddItem(item);
        m_actualizarInventario.Raise();
    }

    public void ConsumeItem(SOItems item)
    {
        m_inventarioSO.RemoveItem(item);
        m_actualizarInventario.Raise();
    }

    public void ActualizarInventario()
    {
        LimpiarInventario();
        RellenarInventario();
    }

    //-------------------------------[ FUNCIONES ]-------------------------------//
    public void LimpiarInventario()
    {
        print("limpiar");
        foreach (Transform child in m_inventarioContent.transform)
            Destroy(child.gameObject);
    }

    private void RellenarInventario()
    {
        foreach (InventorySO.ItemSlot itemSlot in m_inventarioSO.ItemSlots)
        {
            GameObject itemMostrado = Instantiate(m_inventorySlotPrefab, m_inventarioContent.transform);
            itemMostrado.GetComponent<MostrarItem>().Load(itemSlot);
        }
    }

    public void MostrarInventario()
    {
        if (m_inventarioContent.transform.childCount > 0)
            EventSystem.current.SetSelectedGameObject(m_inventarioContent.transform.GetChild(0).gameObject);

        m_Input.FindActionMap("Inventory").Enable();
        ActivarInventario(true);
    }

    public void CerrarInventario(InputAction.CallbackContext actionContext)
    {
        m_Input.FindActionMap("Inventory").Disable();
        ActivarInventario(false);
        inventarioCerrado.Raise();
        this.gameObject.GetComponent<SelectedItem>().CerrarInventario();
    }

    private void ActivarInventario(bool activo)
    {
        if (this != null)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(activo);
            }

        }
    }
}
