using System.Collections;
using UnityEngine;

public class DamageManager : MonoBehaviour
{
    [Header("Parámetros Damage Manager")]
    [SerializeField]
    private int enemyHP;
    [SerializeField]
    private bool m_OnFire;
    public bool OnFire { get => m_OnFire; set => m_OnFire = value; }
    

    [Header("Scriptable objects")]
    private ScriptableObject m_SO;
    public ScriptableObject SO { get => m_SO; set => m_SO = value; }

    [Header("Eventos")]
    public GameEvent OnTakeDamage;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Start()
    {
        if (m_SO is PlayerSO p)
        {
            m_SO = p;
        }
        else if (m_SO is SOEnemy e)
        {
            m_SO = e;
            enemyHP = e.hp;
        }
    }
    private void OnEnable()
    {
        if (m_SO is SOEnemy e)
        {
            m_SO = e;
            enemyHP = e.hp;
        }
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator Quemado(int dmg)
    {
        m_OnFire = true;

        for (int i = 0; i < 5; i++)
        {
            if (m_SO is PlayerSO player)
            {
                player.vida -= dmg / 2;
                OnTakeDamage.Raise();
                if (player.vida <= 0 && this.gameObject.tag == "Player1") this.gameObject.GetComponent<PlayerController>().LlamarAMuerte();
                if (player.vida <= 0 && this.gameObject.tag == "Player2") this.gameObject.GetComponent<Player2Controller>().LlamarAMuerte();
            }
            else if (m_SO is SOEnemy e)
            {
                enemyHP -= dmg / 2;
                if (enemyHP <= 0 && this.gameObject.tag == "Boss") this.gameObject.GetComponent<BossController>().LlamarAMuerte();
                if (enemyHP <= 0 && this.gameObject.tag == "EnemyMelee") this.gameObject.GetComponent<MeleeEnemyController>().LlamarAMuerte();
                if (enemyHP <= 0 && this.gameObject.tag == "EnemyRange") this.gameObject.GetComponent<RangedEnemyController>().LlamarAMuerte();

            }
            yield return new WaitForSeconds(1);
        }
        m_OnFire = false;
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    public bool LoseHealth(int dmg, DamageTypes.DamageTypesEnum type)
    {
        switch (type)
        {
            case DamageTypes.DamageTypesEnum.BASIC: //Da�o directo
                if (m_SO is PlayerSO p)
                {
                    p.vida -= dmg;
                    OnTakeDamage.Raise();
                }
                else if (m_SO is SOEnemy en)
                {
                    enemyHP -= dmg;
                }
                break;
            case DamageTypes.DamageTypesEnum.FIRE: //Da�o directo + quemado
                if (m_SO is PlayerSO player)
                {
                    player.vida -= dmg;
                    OnTakeDamage.Raise();
                    if (player.vida <= 0)
                        return true;
                }
                else if (m_SO is SOEnemy en)
                {
                    enemyHP -= dmg;
                    if (enemyHP <= 0)
                        return true;
                }
                if (m_OnFire) //Refrescar el quemado
                {
                    StopAllCoroutines();
                    StartCoroutine(Quemado(dmg));
                }
                else //Quemar al enemigo
                    StartCoroutine(Quemado(dmg));
                break;

        }
        if (m_SO is PlayerSO pl && pl.vida <= 0)
        {
            return true;
        }
        if (m_SO is SOEnemy e && enemyHP <= 0)
        {
            return true;
        }
        return false;

    }

    public void curar()
    {
        OnTakeDamage.Raise();
    }
}
