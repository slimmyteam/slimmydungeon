using UnityEngine;
using static DamageTypes;

public class HitboxController : MonoBehaviour
{
    [Header("Parámetros Hitbox Controller")]
    public int damage = 0; //attack damage
    public DamageTypesEnum type = DamageTypesEnum.BASIC;

}
