using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;

    [Header("Scriptable Objects")]
    [SerializeField]
    private ShopSlotSO m_ShopStock;
    [SerializeField]
    private PartidaSO m_Partida;
    [SerializeField]
    private SOItems m_NormalPotion;
    [SerializeField]
    private SOItems m_Revive;
    [SerializeField]
    private SOItems m_SpeedPotion;
    [SerializeField]
    private SOItems m_Boomerang;
    [SerializeField]
    private SOItems m_Firestone;
    [SerializeField]
    private SOItems m_FragmentoSinLuz;

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_ShopContent;
    [SerializeField]
    private GameObject m_ShopSlot;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_TiendaCerrada;
    [SerializeField]
    private GEItem m_ComprarItem;
    [SerializeField]
    private GameEvent actualizarCoins;
    [SerializeField]
    private GEGenericoString m_lanzarMensaje;

    public ShopSlotSO ShopStock { get { return m_ShopStock; } set { m_ShopStock = value; } }

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Input = Instantiate(m_InputActionAsset);
        m_Input.FindActionMap("Inventory").FindAction("Exit").performed += CloseShop;
        m_Input.FindActionMap("Inventory").FindAction("Submit").performed += OnSubmit;

    }
    void Start()
    {
        RellenarTienda();
    }

    //----------------------------------[ INPUTS }----------------------------------//

    private void OnSubmit(InputAction.CallbackContext actionContext)
    {
        GameObject slot = EventSystem.current.currentSelectedGameObject;
        Sprite selectedGameObject = slot.GetComponent<Image>().sprite;
        SOItems itemSeleccionado;
        print(selectedGameObject.name);

        switch (selectedGameObject.name)
        {
            case "NormalPotion":
                itemSeleccionado = m_NormalPotion;
                break;
            case "Revive":
                itemSeleccionado = m_Revive;
                break;
            case "SpeedPotion":
                itemSeleccionado = m_SpeedPotion;
                break;
            case "Boomerang":
                itemSeleccionado = m_Boomerang;
                break;
            case "FireStone":
                itemSeleccionado = m_Firestone;
                break;
            case "fragminto sin luz":
                itemSeleccionado = m_FragmentoSinLuz;
                break;
            default:
                itemSeleccionado = m_NormalPotion;
                break;

        }
        if (m_Partida.money - itemSeleccionado.Cost >= 0)
        {
            m_Partida.money -= itemSeleccionado.Cost;
            m_ComprarItem.Raise(itemSeleccionado);
            actualizarCoins.Raise();
        }
        else m_lanzarMensaje.Raise("Este objeto es demasiado caro para ti, vuelve cuando tengas m�s dinero");
    }

    public void CloseShop(InputAction.CallbackContext actionContext)
    {
        m_Input.FindActionMap("Inventory").Disable();
        ActivateShop(false);
        m_TiendaCerrada.Raise();
    }

    //----------------------------------[ EVENTOS }----------------------------------//

    public void OpenShop()
    {
        if (m_ShopContent.transform.childCount > 0)
            EventSystem.current.SetSelectedGameObject(m_ShopContent.transform.GetChild(0).gameObject);

        m_Input.FindActionMap("Inventory").Enable();
        ActivateShop(true);
    }

    //----------------------------------[ FUNCIONES }----------------------------------//
    private void ActivateShop(bool activo)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(activo);
        }

    }

    private void RellenarTienda()
    {
        foreach (ShopSlotSO.ShopItem shopSlot in m_ShopStock.shopItem)
        {
            GameObject itemMostrado = Instantiate(m_ShopSlot, m_ShopContent.transform);
            itemMostrado.GetComponent<MostrarItem>().Load(shopSlot);
        }
    }
}
