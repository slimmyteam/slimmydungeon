using UnityEngine;

public class SellerController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    private int m_PlayerLayer;

    [Header("Variables de control")]
    private bool m_ShopAvailable;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_TiendaAbierta;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_PlayerLayer = LayerMask.NameToLayer("Player");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerLayer)
            m_ShopAvailable = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerLayer)
            m_ShopAvailable = false;
    }

    //----------------------------------[ EVENTOS }----------------------------------//
    public void AbrirTienda()
    {
        if (m_ShopAvailable)
        {
            m_TiendaAbierta.Raise();
        }

    }
}
