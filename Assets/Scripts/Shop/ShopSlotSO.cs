using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/ShopSlot")]
public class ShopSlotSO : ScriptableObject
{
    [SerializeField]
    private List<ShopItem> m_ShopItem = new List<ShopItem>();
    public List<ShopItem> shopItem { get { return m_ShopItem; } set { m_ShopItem = value; } }

    [Serializable] //Esto hace que lo podamos ver por el inspector
    public class ShopItem
    {
        [SerializeField]
        public SOItems item;

        //Constructor
        public ShopItem(SOItems soItem)
        {
            item = soItem;
        }
    }
}
