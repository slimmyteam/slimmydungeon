public interface IUsable
{
    public void Heal(int healAmount, bool revive);
    public void StatBuff(string statName, float buffAmount);
    public void TempStatBuff(string statName, float buffAmount, int durationSec);
}

public interface IEquipable
{
    public void PowerRock(DamageTypes.DamageTypesEnum tipoDano, SOItems item);
    public void AdvanceShoot(AdvanceShootsTypes.AdvanceShootsEnum tipoDisp, SOItems item);
}