using UnityEngine;

public interface IItem
{
    public Sprite Sprite { get; set; }
    public ItemTypes.ItemTypesEnum ItemTypes { get; set; }
    public int Value { get; set; }


    public bool UsedBy(GameObject go);
}
