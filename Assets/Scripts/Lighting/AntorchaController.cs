using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class AntorchaController : MonoBehaviour
{
    [Header("Componentes")]
    private Light2D m_Light2D;

    [Header("Parámetros antorcha")]
    [SerializeField]
    private float minIntensity;
    [SerializeField]
    private float maxIntensity;
    [SerializeField]
    private float minSecondsFlicker;
    [SerializeField]
    private float maxSecondsFlicker;

    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Light2D = GetComponent<Light2D>();
    }

    void OnEnable()
    {
        StartCoroutine(LightFlickering());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator LightFlickering()
    {
        while (true)
        {
            float intensidadAnterior = m_Light2D.intensity;
            float randomIntensity = Random.Range(minIntensity, maxIntensity);
            float tiempo = Random.Range(minSecondsFlicker, maxSecondsFlicker);
            float currentTime = 0f;
            while (currentTime <= tiempo)
            {
                if (intensidadAnterior > randomIntensity && intensidadAnterior <= maxIntensity)
                    m_Light2D.intensity = Mathf.Lerp(intensidadAnterior, minIntensity, currentTime / tiempo);
                yield return null;
                currentTime += Time.deltaTime;
            }
            intensidadAnterior = m_Light2D.intensity;
            currentTime = 0f;
            while (currentTime <= tiempo)
            {
                if (intensidadAnterior >= minIntensity && intensidadAnterior < randomIntensity)
                    m_Light2D.intensity = Mathf.Lerp(intensidadAnterior, maxIntensity, currentTime / tiempo);
                yield return null;
                currentTime += Time.deltaTime;
            }
            yield return new WaitForSeconds(tiempo);
        }
    }
}
