using UnityEngine;

[CreateAssetMenu(fileName = "Power Rock Item", menuName = "Scriptable Objects/Items/Power Rock")]
public class PowerRockItem : SOItems
{
    [Header("Parámetros Power Rock Item")]
    [SerializeField]
    private DamageTypes.DamageTypesEnum m_damgeType;
    public override ItemTypes.ItemTypesEnum ItemTypes { get => m_itemType; set => m_itemType = value; }

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IEquipable equipable))
            return false;

        equipable.PowerRock(m_damgeType, this);
        return true;
    }
}

