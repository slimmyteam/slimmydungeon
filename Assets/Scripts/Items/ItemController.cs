using UnityEngine;

public class ItemController : MonoBehaviour
{
    [Header("Referencias a objetos")]
    [SerializeField]
    private SOItems m_itemSO;
    [SerializeField]
    public SOItems ItemSO { get => m_itemSO; }


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        GetComponent<SpriteRenderer>().sprite = m_itemSO.Sprite;
    }
}
