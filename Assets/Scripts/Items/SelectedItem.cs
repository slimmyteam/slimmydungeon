using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SelectedItem : MonoBehaviour
{
    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_SubmitAction;
    private InputAction m_CancelAction;

    [Header("Referencias a objetos")]
    [SerializeField]
    private GameObject m_Player1;
    [SerializeField]
    private GameObject m_Player2;
    [SerializeField]
    private GameObject m_Player1Prefab;
    [SerializeField]
    private GameObject m_Player2Prefab;
    [SerializeField]
    private GameObject m_objetoAEntregar;
    [SerializeField]
    private GameObject m_Inventory;
    [SerializeField]
    private GameObject m_InventoryContent;

    [Header("Corutinas")]
    private Coroutine m_esperaParaSetear;


    //----------------------------------[ UNITY EVENT FUNCTIONS }----------------------------------//
    private void Awake()
    {
        m_Input = Instantiate(m_InputActionAsset);

    }
    private void OnEnable()
    {
        m_SubmitAction = m_Input.FindActionMap("Inventory").FindAction("Submit");
        m_CancelAction = m_Input.FindActionMap("Inventory").FindAction("Cancel");
        m_SubmitAction.performed += onSubmit;
        m_CancelAction.performed += OnCancel;

        m_Player1.GetComponent<Button>().interactable = false;
        m_Player2.GetComponent<Button>().interactable = false;
        m_objetoAEntregar = null;
    }
    private void OnDisable()
    {
        m_SubmitAction.performed -= onSubmit;
        m_CancelAction.performed -= OnCancel;
        if (m_esperaParaSetear != null) StopCoroutine(m_esperaParaSetear);
    }

    //----------------------------------[ INPUTS }----------------------------------//
    private void OnCancel(InputAction.CallbackContext context)
    {
        InitValues();
    }

    private void onSubmit(InputAction.CallbackContext context)
    {
        if (m_objetoAEntregar != null && EventSystem.current.currentSelectedGameObject.name.Contains("Player"))
        {
            GameObject playerRecibe;

            if (EventSystem.current.currentSelectedGameObject.name == "Player1Image")
            {
                playerRecibe = m_Player1Prefab;
            }
            else
            {
                playerRecibe = m_Player2Prefab;
            }

            m_objetoAEntregar.GetComponent<MostrarItem>().Item.UsedBy(playerRecibe);
            GetComponent<InventoryController>().ConsumeItem(m_objetoAEntregar.GetComponent<MostrarItem>().Item);
            m_esperaParaSetear = StartCoroutine(esperaParaSetear());
            return;
        }

        m_objetoAEntregar = EventSystem.current.currentSelectedGameObject;

        SetearObjetoSeleccionado(m_Player1);
        ActivacionPlayersEInventario(true);
    }


    //----------------------------------[ CORUTINAS }----------------------------------//
    private IEnumerator esperaParaSetear()
    {
        //Waiting a frame before selecting the button (para solucionar que en el onSubmit se vuelvan a poder seleccionar objetos). La ejecucion de esta funcion se detiene temporalmente y se reanuda en el siguiente frame
        yield return null;
        InitValues();
    }

    //----------------------------------[ FUNCIONES }----------------------------------//

    private void InitValues()
    {
        ActivacionPlayersEInventario(false);
        if (m_objetoAEntregar == null) SetearObjetoSeleccionado(m_InventoryContent.transform.GetChild(0).gameObject);
        else SetearObjetoSeleccionado(m_objetoAEntregar);
    }

    private void SetearObjetoSeleccionado(GameObject gameObject)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }

    private void ActivacionPlayersEInventario(bool activo)
    {
        m_Player1.GetComponent<Button>().interactable = activo;
        m_Player2.GetComponent<Button>().interactable = activo;
        m_Inventory.SetActive(!activo);
    }

    public void MostrarInventario()
    {
        m_Input.FindActionMap("Inventory").Enable();
    }

    public void CerrarInventario()
    {
        m_Input.FindActionMap("Inventory").Disable();
    }
}
