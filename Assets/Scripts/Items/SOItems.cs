using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Scriptable Objects", menuName = "Scriptable Objects/Items")]
public abstract class SOItems : ScriptableObject, IItem
{
    [SerializeField] 
    private int _ID;
    public int ID => _ID;
    [Header("Common fields")]
    [SerializeField]
    protected Sprite m_Sprite = null;
    [SerializeField]
    protected ItemTypes.ItemTypesEnum m_itemType;
    [SerializeField]
    [Min(1)]
    protected int m_value = 0;
    [SerializeField]
    private int m_Cost;

    public abstract bool UsedBy(GameObject go);
    public abstract ItemTypes.ItemTypesEnum ItemTypes { get; set; }
    public Sprite Sprite { get => m_Sprite; set => m_Sprite = value; }
    public ItemTypes.ItemTypesEnum ItemType { get => m_itemType; set => m_itemType = value; }
    public int Value { get => m_value; set => m_value = value; }
    public int Cost { get => m_Cost; set => m_Cost = value; }
}
