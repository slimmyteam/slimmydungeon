using UnityEngine;

[CreateAssetMenu(fileName = "Healing Item", menuName = "Scriptable Objects/Items/Healing")]
public class HealingItem : SOItems
{
    [Header("Parámetros Healing Item")]
    [SerializeField]
    private bool m_revive;
    public override ItemTypes.ItemTypesEnum ItemTypes { get => m_itemType; set => m_itemType = value; }


    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IUsable usable))
            return false;

        usable.Heal(Value, m_revive);
        return true;

    }


}
