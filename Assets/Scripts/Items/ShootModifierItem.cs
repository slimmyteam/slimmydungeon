using UnityEngine;

[CreateAssetMenu(fileName = "Shoot Mofifier Item", menuName = "Scriptable Objects/Items/Shoot Modifier")]

public class ShootModifierItem : SOItems
{
    [Header("Parámetros Shoot Modifier Item")]
    [SerializeField]
    private AdvanceShootsTypes.AdvanceShootsEnum m_shootType;

    public override ItemTypes.ItemTypesEnum ItemTypes { get => m_itemType; set => m_itemType = value; }

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IEquipable equipable))
            return false;

        equipable.AdvanceShoot(m_shootType, this);
        return true;
    }
}
