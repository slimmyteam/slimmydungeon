using UnityEngine;

public class ItemTypes : MonoBehaviour
{
    public enum ItemTypesEnum
    {
        FIRESTONE, NORMALSTONE, NORMALPOTION, STRENGTHPOTION, SPEEDPOTION, REVIVE, BOOMERANG, FRAGMENTOSINLUZ
    }
}
