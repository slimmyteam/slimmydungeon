using UnityEngine;

[CreateAssetMenu(fileName = "Stat Buff Item", menuName = "Scriptable Objects/Items/Stat Buff")]

public class StatBuffItem : SOItems
{
    public override ItemTypes.ItemTypesEnum ItemTypes { get => m_itemType; set => m_itemType = value; }
    [Header("Parámetros Buff Item values")]
    [SerializeField]
    private string m_statName;

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IUsable usable))
            return false;

        usable.StatBuff(m_statName, Value);
        return true;
    }
}
