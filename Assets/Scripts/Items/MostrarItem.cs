using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MostrarItem : MonoBehaviour
{
    [Header("Scriptable objects")]
    [SerializeField]
    private SOItems m_item;
    [SerializeField]
    public SOItems Item { get => m_item; }

    [Header("Referencias a objetos")]
    [SerializeField]
    private TextMeshProUGUI m_AmountText;
    [SerializeField]
    private Image m_Image;

    public void Load(SOItems item)
    {
        m_item = item;
        m_Image.sprite = item.Sprite;
    }

    public void Load(InventorySO.ItemSlot itemSlot)
    {
        Load(itemSlot.item);
        m_AmountText.text = itemSlot.amount.ToString();
    }
    public void Load(ShopSlotSO.ShopItem itemSlot)
    {
        Load(itemSlot.item);
        m_AmountText.text = itemSlot.item.Cost.ToString() + " C";
    }
}
