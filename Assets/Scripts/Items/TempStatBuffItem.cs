using UnityEngine;


[CreateAssetMenu(fileName = "Temp Stat Buff Item", menuName = "Scriptable Objects/Items/Temp Stat Buff")]

public class TempStatBuffItem : SOItems
{
    [Header("Parámetros Temp Stat Buff Item")]
    [SerializeField]
    [Min(1f)]
    private float m_buffAmount;
    [SerializeField]
    private string m_statName;
    [SerializeField]
    private int m_durationSec;

    public override ItemTypes.ItemTypesEnum ItemTypes { get => m_itemType; set => m_itemType = value; }


    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IUsable usable))
            return false;

        usable.TempStatBuff(m_statName, m_buffAmount, m_durationSec);
        return true;
    }
}
